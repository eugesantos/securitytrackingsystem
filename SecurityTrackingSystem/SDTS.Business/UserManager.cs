﻿using SDTS.Data;
using SDTS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDTS.Business
{
    public class UserManager
    {
        public readonly Data.Interface.IUserDataProvider _userDataProvider;

        public UserManager()
        {
            _userDataProvider = new Data.Implementation.UserDataProvider();
        }

        public int AddUser(UserInformationModel userInfo)
        {
            try
            {
                int userId = 0;

                if (userInfo != null)
                    userId = _userDataProvider.AddUser(userInfo);

                return userId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
