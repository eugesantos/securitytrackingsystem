﻿using SDTS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDTS.Data.Interface
{
    public interface IUserDataProvider
    {
        int AddUser(UserInformationModel userInfo);
    }
}
