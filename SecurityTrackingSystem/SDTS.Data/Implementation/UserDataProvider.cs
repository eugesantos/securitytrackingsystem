﻿using SDTS.Data.Interface;
using SDTS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDTS.Data.Implementation
{
    public class UserDataProvider : IUserDataProvider
    {
        private SDTSEntities _DbEntities;

        public UserDataProvider()
        {
            _DbEntities = new SDTSEntities();
        }

        int IUserDataProvider.AddUser(UserInformationModel userInfo)
        {
            if (userInfo == null)
                return 0;

            UserInformation userInformation = new UserInformation()
            {
                ui_FirstName = userInfo.FirstName,
                ui_MiddleName = userInfo.MiddleName,
                ui_LastName = userInfo.LastName,
                ui_Gender = userInfo.Gender,
                ui_Birthdate = userInfo.Birthdate,
                ui_ContactNo = userInfo.ContactNo
            };

            _DbEntities.UserInformations.Add(userInformation);
            _DbEntities.SaveChanges();

            return userInformation.ui_ID;
        }
    }
}
