//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SDTS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserInformation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserInformation()
        {
            this.UserAddresses = new HashSet<UserAddress>();
            this.UserLogins = new HashSet<UserLogin>();
            this.UsersRoles = new HashSet<UsersRole>();
        }
    
        public int ui_ID { get; set; }
        public string ui_FirstName { get; set; }
        public string ui_MiddleName { get; set; }
        public string ui_LastName { get; set; }
        public string ui_Gender { get; set; }
        public Nullable<System.DateTime> ui_Birthdate { get; set; }
        public Nullable<int> ui_ContactNo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserAddress> UserAddresses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserLogin> UserLogins { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsersRole> UsersRoles { get; set; }
    }
}
