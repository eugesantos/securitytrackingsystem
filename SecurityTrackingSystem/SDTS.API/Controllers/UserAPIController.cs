﻿using SDTS.Business;
using SDTS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SDTS.API.Controllers
{
    [RoutePrefix("api/user")]
    public class UserAPIController : ApiController
    {
        UserManager _userManager;
        public UserAPIController()
        {
            _userManager = new UserManager();
        }

        [HttpPost]
        [Route("add")]
        public int AddUser(UserInformationModel userInfo)
            => _userManager.AddUser(userInfo);
    }
}